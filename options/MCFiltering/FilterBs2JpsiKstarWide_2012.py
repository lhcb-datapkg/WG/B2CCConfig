"""
Stripping Filtering file for the Bs2JpsiKstarWideLine. Prescales removed.
@author Frank Meier
@date   2015-02-03

"""

from Gaudi.Configuration import *
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

#
# Build the stream and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStream, cloneLinesFromStream
from StrippingArchive import strippingArchive

stripping='stripping20r0p1'
#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

_dimuon          = buildStream(stripping=config, streamName='Dimuon', archive=archive)
_dimuon.lines[:] = [ x for x in _dimuon.lines if 'Bs2JpsiKstarWide' in x.name() ]

for line in _dimuon.lines :
   print "Dimuon has a line called " + line.name()

# turn off all pre-scalings 
for line in _dimuon.lines:
  line._prescale = 1.0

#
# Merge into one stream and run in flag mode
#
AllStreams = StrippingStream("Bs2JpsiKstar.Strip")
AllStreams.appendLines(_dimuon.lines)

sc = StrippingConf( HDRLocation = "NotExistingLocation",
                    Streams = [ AllStreams ],
                    MaxCandidates = 2000 )

AllStreams.sequence().IgnoreFilterPassed = False # filtering mode

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                              stripDSTStreamConf,
                                              stripDSTElements
                                              )


#
# Configuration of SelDSTWriter
#
SelDSTWriterElements = {
    'default'              : stripDSTElements()
    }


SelDSTWriterConf = {
    'default'              : stripDSTStreamConf()
    }

for stream in sc.activeStreams() :
   print "there is a stream called " + stream.name() + " active"

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = sc.activeStreams()
                          )

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().InputType = 'DST'
DaVinci().DataType = '2012'
DaVinci().Simulation = True
DaVinci().EvtMax = -1
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )