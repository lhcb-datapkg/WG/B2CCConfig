from OfflineSelections import OfflineSelection

class Bu2JpsiKUnbiased( OfflineSelection ):
    def __init__( self, selection = None ):
        OfflineSelection.__init__( self, "Bu2JpsiK" )
        
        self._extraVars = { "K_Pt"   : "CHILD( PT, 2 )",
                            "K_P"    : "CHILD( P, 2 )" }

        self._cut = "( ( '%(MotherName)s' == ABSID ) \
        & ( DTF_CHI2NDOF( True ) < %(maxBDTFChi2DoF)s   ) )"
       
        self._selection = { "MotherName"       :  "B+",
                            "maxBDTFChi2DoF"   :     25 }
        
        if selection:
            self._selection.update( selection )

class Bs2JpsiPhiUnbiased( OfflineSelection ):
    def __init__( self, selection = None ):
        OfflineSelection.__init__( self, "Bs2JpsiPhi" )
        self._angles = True
        self._angleTool = "Bs2JpsiPhiAngleCalculator"

        self._extraVars = { "phi_Mass" : "CHILD( M, 2 )",
                            "phi_Pt"   : "CHILD( PT, 2 )",
                            "phi_P"    : "CHILD( P, 2 )" }

        self._cut = "( ( '%(MotherName)s' == ABSID ) \
        & ( DTF_CHI2NDOF( True ) < %(maxBDTFChi2DoF)s   ) )"

        self._selection = { "MotherName"       : "B_s0",
                            "maxBDTFChi2DoF"   :      25 }

        if selection:
            self._selection.update( selection )

    def angleTool( self ):
        return self._angleTool

class Bd2JpsiKstarUnbiased( OfflineSelection ):
    def __init__( self, selection = None ):
        OfflineSelection.__init__( self, "Bd2JpsiKstar" )
        self._angles = True
        self._angleTool = "Bd2KstarMuMuAngleCalculator"

        self._extraVars = { "Kstar_Mass" : "CHILD( M, 2 )",
                            "Kstar_Pt"   : "CHILD( PT, 2 )",
                            "Kstar_P"    : "CHILD( P, 2 )" }

        self._cut = "( ( '%(MotherName)s' == ABSID )                                                 \
                & ( DTF_CHI2NDOF( True ) < %(maxBDTFChi2DoF)s    ) )"
        
        self._selection = { "MotherName"        :  "B0",
                            "maxBDTFChi2DoF"    :     25 }

        if selection:
            self._selection.update( selection )

    def angleTool( self ):
        return self._angleTool

class Bd2JpsiKsUnbiased( OfflineSelection ):
    def __init__( self, selection = None ):
        OfflineSelection.__init__( self, "Bd2JpsiKs" )
        self._angles = True

        self._cut = "( ( '%(MotherName)s' == ABSID ) \
        & ( DTF_CHI2NDOF( True )                 < %(maxBDTFChi2DoF)s         ) )"

        self._selection = { "MotherName"       :  "B0",
                            "maxBDTFChi2DoF"   :     25 }

        if selection:
            self._selection.update( selection )

class Lambdab2JpsiLambdaUnbiased( OfflineSelection ):
    def __init__( self, selection = None ):
        OfflineSelection.__init__( self, "Bd2JpsiKs" )
        self._angles = True

        self._cut = "( ( '%(MotherName)s' == ABSID )                                                \
        & ( DTF_CHI2NDOF( True )                                         < %(maxLambdaBDTFChi2DoF)s ) )"

        self._selection = { "MotherName"           : "Lambda_b0",
                            "maxLambdaBDTFChi2DoF" :           25 }

        if selection:
            self._selection.update( selection )
